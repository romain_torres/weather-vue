import { mount, flushPromises } from "@vue/test-utils";
import Dashboard from "../../components/ui/Dashboard.vue";
import axios from "axios";
import jsonMockData from "./mockData/weather.json";
import API from "../../api/weather";

//mock to be TypeScript compatible
jest.mock("axios");
const mockedAxios = axios as jest.Mocked<typeof axios>;
//jest.Mocked<typeof axios> avoid TypeScript error

test("Metaweather API call succeed when dashboard is mounted", async () => {
  mockedAxios.get.mockImplementationOnce(() =>
    Promise.resolve({
      data: jsonMockData,
    })
  );

  const wrapper = mount(Dashboard, { props: { daysNumber: 6 } });

  expect(mockedAxios.get).toHaveBeenCalledTimes(1);

  expect(wrapper.find(".loading").exists()).toBe(true);

  await flushPromises();

  expect(wrapper.find(".loading").exists()).toBe(false);

  expect(wrapper.findAll('[data-test="day"]')).toHaveLength(6);
});

test("Metaweather API call not succeed and app show error", async () => {
  const errorMessage = new Error(
    "Metaweather API is not responding or a network error happen."
  );

  mockedAxios.get.mockImplementationOnce(() => Promise.reject(errorMessage));

  const wrapper = mount(Dashboard, { props: { daysNumber: 6 } });

  expect(mockedAxios.get).toHaveBeenCalledTimes(1);

  expect(wrapper.find(".loading").exists()).toBe(true);

  await flushPromises();
  // implement a way in the app to show error from the api
  expect(wrapper.find(".loading").exists()).toBe(false);

  expect(wrapper.find(".error").exists()).toBe(true);
});

test("Metaweather API call failed", async () => {
  const errorMessage = "Network Error";
  mockedAxios.get.mockImplementationOnce(() =>
    Promise.reject(new Error(errorMessage))
  );
  const api = new API();

  await expect(api.fetchWeatherDataDefaultLocation()).rejects.toThrow(
    errorMessage
  );
});

afterEach(() => {
  jest.clearAllMocks();
});
