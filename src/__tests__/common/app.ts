import { mount } from "@vue/test-utils";
import App from "../../App.vue";

test("App.vue", async () => {
  const wrapper = mount(App);

  expect(wrapper.find(".header").exists()).toBeTruthy()
});
