import API from "../../api/weather";
import { ref, onMounted, watch } from "vue";
import moment from "moment";

export default function useWeatherApi() {
  const api = new API();
  const weatherData = ref<WeatherResponse>();
  const userCity = ref("");
  const fetchWeatherData = async () => {
    try {
      const days = await api.fetchWeatherDataDefaultLocation();
      weatherData.value = parseWeatherData(days.data.consolidated_weather);
    } catch (error) {
      weatherData.value = { success: false, days: [], error: error };
    }
  };

  const fetchWeatherUserData = async (lat: number, lng: number) => {
    try {
      const locations = await api.fetchWeatherUserDataLocation(lat, lng);
      const closestCity = await getClosestCity(locations.data);

      userCity.value = closestCity.title;
      const days = await api.fetchWeatherUserData(closestCity.woeid);

      weatherData.value = parseWeatherData(days.data.consolidated_weather);
    } catch (error) {
      weatherData.value = { success: false, days: [], error: error };
    }
  };

  const parseWeatherData = (data: any) => {
    let daysArray: Day[] = [];
    let dataParsed: WeatherResponse = {
      success: true,
      days: daysArray
    };

    data.forEach((element: any) => {
      let elParsed: Day = {
        min_temp: Math.floor(element.min_temp),
        max_temp: Math.floor(element.max_temp),
        wind_speed: Math.floor(element.wind_speed),
        applicable_date: element.applicable_date,
        humidity: element.humidity,
        state: element.weather_state_abbr,
        wind_direction: element.wind_direction,
        wind_direction_compass: element.wind_direction_compass,
      };

      daysArray.push(elParsed);
    });

    dataParsed.days = daysArray;

    return dataParsed;
  };

  const getClosestCity = (data: any) => {
    return data[0];
  };

  onMounted(fetchWeatherData);

  return {
    userCity,
    weatherData,
    fetchWeatherData,
    fetchWeatherUserData,
  };
}
