import axios from "axios";
import { cacheAdapterEnhancer } from "axios-extensions";
/*  
const http = axios.create({
    baseURL: '/',
    headers: { 'Cache-Control': 'no-cache' },
    // cache will be enabled by default
    adapter: cacheAdapterEnhancer(axios.defaults.adapter as any)
}); */
const API_DEFAULT_ENDPOINT = "/api/location/";
const API_SEARCH_ENDPOINT = "/api/location/search/";

export default class API {
  /**
   *
   * @returns json data from metaweather.co (default location : Lisbon)
   * @env API_DEFAULT_ENDPOINT : url of the api
   */
  async fetchWeatherDataDefaultLocation() {
    try {
      return await axios.get(
        API_DEFAULT_ENDPOINT + '742676/'
      );
    } catch (error) {
      //throw new Error(error);
      return Promise.reject(error);
    }
  }

  async fetchWeatherUserDataLocation(lat: number, lng: number) {
    try {
      return await axios.get(
        API_SEARCH_ENDPOINT + "?lattlong=" + lat.toString() + "," + lng.toString()
      );
    } catch (error) {
      return error;
    }
  }

  async fetchWeatherUserData(woeid: number) {
    try {
      return await axios.get(API_DEFAULT_ENDPOINT + woeid + '/');
    } catch (error) {
      return error;
    }
  }
}
