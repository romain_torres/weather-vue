interface Day {
  min_temp: number;
  max_temp: number;
  wind_speed: number;
  wind_direction: number;
  wind_direction_compass: string;
  humidity: number;
  applicable_date: string;
  state: string;
}
interface WeatherResponse {
  days: Array<Day>;
  success: boolean;
  error?: Error;
}
