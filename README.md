# Weather Vue

Weather application using [Vue 3](https://v3.vuejs.org/guide/installation.html) and [Typescript](https://www.typescriptlang.org/docs/handbook/typescript-from-scratch.html) in [Vite](https://vitejs.dev/).

![Coverage: badge branches](badges/badge-lines.svg)

See the result here https://weather-vue.romaintorres.dev/

## Recommended IDE Setup

[VSCode](https://code.visualstudio.com/) + [Vetur](https://marketplace.visualstudio.com/items?itemName=octref.vetur). Make sure to enable `vetur.experimental.templateInterpolationService` in settings!

## Type Support For `.vue` Imports in TS

Since TypeScript cannot handle type information for `.vue` imports, they are shimmed to be a generic Vue component type by default. In most cases this is fine if you don't really care about component prop types outside of templates. However, if you wish to get actual prop types in `.vue` imports (for example to get props validation when using manual `h(...)` calls), you can use the following:

### If Using Volar

Run `Volar: Switch TS Plugin on/off` from VSCode command palette.

### If Using Vetur

1. Install and add `@vuedx/typescript-plugin-vue` to the [plugins section](https://www.typescriptlang.org/tsconfig#plugins) in `tsconfig.json`
2. Delete `src/shims-vue.d.ts` as it is no longer needed to provide module info to Typescript
3. Open `src/main.ts` in VSCode
4. Open the VSCode command palette
5. Search and run "Select TypeScript version" -> "Use workspace version"

## Installation/Setup

```
npm i
```
### Test units
```
npm test
```
### Badges
For now you have to manually type into the terminal this command :
```
npm run test:badges
```

## Librairies

### CSS
#### [Tailwind CSS](https://tailwindcss.com/docs)

### JS
#### [Moment.js](https://momentjs.com/docs/)

#### Axios
#### [Axios](https://axios-http.com/docs/intro/) 

#### Icons
All icons are free from [Weather Icons
](https://bas.dev/projects/weather-icons) 

## Logo

The logo was created using simple shapes thanks to Figma

<img src="src/assets/logo.svg" alt="drawing" width="50"/>
