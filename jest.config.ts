import type { Config } from "@jest/types";

// Sync object
const config: Config.InitialOptions = {
  collectCoverage: true,
  coverageReporters: ["json-summary", "text", "lcov"],
  collectCoverageFrom: [
    "src/**/*.{js,vue,ts}",
    "!**/node_modules/**",
    "!dist",
    "!src/main.ts",
  ],
  coverageThreshold: {
    global: {
      branches: 80,
      functions: 80,
      lines: 80,
      statements: -10,
    },
  },
  preset: "ts-jest",
  testEnvironment: "jsdom",
  transform: {
    "^.+\\.vue$": "vue-jest",
    "^.+\\js$": "babel-jest",
  },
  moduleFileExtensions: ["vue", "js", "json", "jsx", "ts", "tsx", "node"],
};

export default config;
