module.exports = {
  purge: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        pantone: "#E2BBC4"
      },
      maxWidth: {
        wind: "140px"
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
