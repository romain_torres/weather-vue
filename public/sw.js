// sw.js
import { precacheAndRoute } from "workbox-precaching";
import { registerRoute } from "workbox-routing";
import { CacheFirst, NetworkFirst } from "workbox-strategies";

// self.__WB_MANIFEST is default injection point
precacheAndRoute(self.__WB_MANIFEST);

self.addEventListener("message", (event) => {
  if (event.data && event.data.type === "SKIP_WAITING") self.skipWaiting();
});

registerRoute(
  new RegExp("api/location/d*"),
  new NetworkFirst({ networkTimeoutSeconds: 3, cacheName: "weather-cache" })
);

registerRoute(
  new RegExp("/images/svg/.*\\.svg"),
  new CacheFirst({ cacheName: "svg-cache" })
);

registerRoute(
  "manifest.webmanifest",
  new CacheFirst({ cacheName: "manifest" })
);
